output "alb_dns_name" {
  value       = module.alb.this_lb_dns_name
  description = "ALB DNS name"
}

output "alb_target_group_arns" {
  value       = module.alb.target_group_arns
  description = "ALB target group ARNs"
}

output "alb_security_group_id" {
  value       = module.alb_security_group.this_security_group_id
  description = "SSH Security Group ID"
}

output "alb_security_group_name" {
  value       = module.alb_security_group.this_security_group_name
  description = "SSH Security Group name"
}
