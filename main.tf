terraform {
  # The configuration for this backend will be filled in by Terragrunt or via a backend.hcl file. See
  # https://www.terraform.io/docs/backends/config.html#partial-configuration
  backend "s3" {}

  # Only allow this Terraform version. Note that if you upgrade to a newer version, Terraform won't allow you to use an
  # older version, so when you upgrade, you should upgrade everyone on your team and your CI servers all at once.
  required_version = "~> 0.12"

  # We use a new ALB Advanced Routing rules format
  required_providers {
    aws = "~> 2.42"
  }
}

provider "aws" {
  region = var.region
}

locals {
  use_https = var.acm_certificate_arn != "" ? true : false

  route53_zone_id          = var.route53_zone_id
  route53_zone_domain_name = var.route53_zone_domain_name
  additional_alb_dns_name  = var.additional_alb_dns_name

  # Removing trailing dot from domain - just to be sure :)
  additional_alb_dns_name_cleaned = trimsuffix(local.additional_alb_dns_name, ".")

  create_aux_alb_dns_condition = (local.route53_zone_domain_name != "" || local.route53_zone_id != "") && (local.additional_alb_dns_name != "")
}

module "alb_security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 3.0"

  name   = "${var.alb_name}-alb-sg"
  vpc_id = var.vpc_id

  # Ingress for HTTP(S)
  ingress_cidr_blocks      = ["0.0.0.0/0"]
  ingress_ipv6_cidr_blocks = ["::/0"]
  ingress_rules            = local.use_https ? ["https-443-tcp", "http-80-tcp"] : ["http-80-tcp"]

  # Allow all egress
  egress_cidr_blocks      = ["0.0.0.0/0"]
  egress_ipv6_cidr_blocks = ["::/0"]
  egress_rules            = ["all-all"]

  tags = var.tags
}

module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 5.0"

  name               = "${var.alb_name}-alb"
  load_balancer_type = "application"
  security_groups    = [module.alb_security_group.this_security_group_id]
  subnets            = var.subnet_ids
  vpc_id             = var.vpc_id

  ip_address_type = var.enable_ipv6 ? "dualstack" : "ipv4"

  listener_ssl_policy_default = var.listener_ssl_policy_default

  http_tcp_listeners = [
    {
      target_group_index = 0
      port               = 80
      protocol           = "HTTP"
    }
  ]

  https_listeners = ! local.use_https ? [] : [
    {
      target_group_index = 0
      port               = 443
      protocol           = "HTTPS"
      certificate_arn    = var.acm_certificate_arn
    }
  ]

  target_groups = [
    {
      name             = "${var.alb_name}-tg"
      backend_protocol = "HTTP"
      backend_port     = var.backend_port
      target_type      = "instance"

      deregistration_delay = var.deregistration_delay

      health_check = var.health_check
    }
  ]

  tags = var.tags
}

resource "aws_lb_listener_rule" "ec2" {
  listener_arn = local.use_https ? module.alb.https_listener_arns[0] : module.alb.http_tcp_listener_arns[0]

  action {
    type             = "forward"
    target_group_arn = module.alb.target_group_arns[0]
  }

  condition {
    path_pattern {
      values = ["/"]
    }
  }
}

resource "aws_lb_listener_rule" "redirect_http_to_https" {
  count = local.use_https ? 1 : 0

  listener_arn = module.alb.http_tcp_listener_arns[0]

  action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }

  condition {
    host_header {
      values = [local.additional_alb_dns_name_cleaned]
    }
  }
}

data "aws_route53_zone" "this" {
  count = local.create_aux_alb_dns_condition ? 1 : 0

  name         = local.route53_zone_domain_name
  private_zone = false
}

resource "aws_route53_record" "alb_aux_dns_name" {
  count = local.create_aux_alb_dns_condition ? 1 : 0

  zone_id = local.route53_zone_id != "" ? local.route53_zone_id : data.aws_route53_zone.this.*.zone_id[0]

  name = local.additional_alb_dns_name_cleaned
  type = "A"

  alias {
    name                   = module.alb.this_lb_dns_name
    zone_id                = module.alb.this_lb_zone_id
    evaluate_target_health = false
  }

  depends_on = [module.alb]
}
