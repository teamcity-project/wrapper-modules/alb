variable "region" {
  description = "AWS region"
  type        = string
}

variable "environment" {
  description = "Environment name"
  type        = string
  default     = ""
}

variable "alb_name" {
  description = "Application load balancer name"
  type        = string
}


variable "vpc_id" {
  description = "The VPC for security group creating"
  type        = string
}

variable "subnet_ids" {
  description = "A list of VPC subnet IDs for ALB"
  type        = list(string)
}

variable "enable_ipv6" {
  description = "Toggle on or off IPv6 support for ALB"
  type        = bool
  default     = false
}

variable "health_check" {
  description = "Map of healthcheck parameters. See [Terraform doc](https://www.terraform.io/docs/providers/aws/r/lb_target_group.html#health_check) for details"
  type        = map(string)
  default     = {}
}

variable "backend_port" {
  description = "The port to use to connect with the target"
  type        = number
}

variable "deregistration_delay" {
  description = "The amount time for Elastic Load Balancing to wait before changing the state of a deregistering target from draining to unused"
  type        = number
  default     = 300
}

variable "route53_zone_id" {
  description = "The ID of Route53 hosted zone to contain new additional DNS record of ALB"
  type        = string
  default     = ""
}

variable "route53_zone_domain_name" {
  description = "The name of Route53 hosted zone to contain new additional DNS record of ALB"
  type        = string
  default     = ""
}

variable "additional_alb_dns_name" {
  description = "Name of additional DNS record of ALB"
  type        = string
  default     = ""
}

variable "acm_certificate_arn" {
  description = "The ARN of the default SSL server certificate"
  type        = string
  default     = null
}

variable "listener_ssl_policy_default" {
  description = "The security policy if using HTTPS externally on the load balancer. [See](https://docs.aws.amazon.com/elasticloadbalancing/latest/classic/elb-security-policy-table.html)."
  type        = string
  default     = "ELBSecurityPolicy-2016-08"
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}
