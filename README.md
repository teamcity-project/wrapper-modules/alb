# ALB wrapper module for Teamcity project

This module is wrapper for [terraform-aws-security-group](https://github.com/terraform-aws-modules/terraform-aws-security-group)
and [terraform-aws-alb](https://github.com/terraform-aws-modules/terraform-aws-alb) modules.

Also, it uses [terraform-aws-ecs by HENNGE](https://github.com/HENNGE/terraform-aws-ecs) module solution.

The module creates:
  - ALB with listeners at 80 (and 443, if ACM SSL certificate specified) port
  - Security group for access to ALB (allow any ingress access to port 80 (and 443))
  - Target group and forward rule with path /
  - Rule for redirect HTTP to HTTPS
  - Additional DNS record with Route53 (optional)

## Requirements
Terraform  0.12

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.42 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| acm\_certificate\_arn | The ARN of the default SSL server certificate | `string` | n/a | yes |
| additional\_alb\_dns\_name | Name of additional DNS record of ALB | `string` | `""` | no |
| alb\_name | Application load balancer name | `string` | n/a | yes |
| backend\_port | The port to use to connect with the target | `number` | n/a | yes |
| deregistration\_delay | The amount time for Elastic Load Balancing to wait before changing the state of a deregistering target from draining to unused | `number` | `300` | no |
| enable\_ipv6 | Toggle on or off IPv6 support for ALB | `bool` | `false` | no |
| environment | Environment name | `string` | `""` | no |
| health\_check | Map of healthcheck parameters. See [Terraform doc](https://www.terraform.io/docs/providers/aws/r/lb_target_group.html#health_check) for details | `map(string)` | `{}` | no |
| listener\_ssl\_policy\_default | The security policy if using HTTPS externally on the load balancer. [See](https://docs.aws.amazon.com/elasticloadbalancing/latest/classic/elb-security-policy-table.html). | `string` | `"ELBSecurityPolicy-2016-08"` | no |
| region | AWS region | `string` | n/a | yes |
| route53\_zone\_domain\_name | The name of Route53 hosted zone to contain new additional DNS record of ALB | `string` | `""` | no |
| route53\_zone\_id | The ID of Route53 hosted zone to contain new additional DNS record of ALB | `string` | `""` | no |
| subnet\_ids | A list of VPC subnet IDs for ALB | `list(string)` | n/a | yes |
| tags | A map of tags to add to all resources | `map(string)` | `{}` | no |
| vpc\_id | The VPC for security group creating | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| alb\_dns\_name | ALB DNS name |
| alb\_security\_group\_id | SSH Security Group ID |
| alb\_security\_group\_name | SSH Security Group name |
| alb\_target\_group\_arns | ALB target group ARNs |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## To Do
* More transparent solution for dealing with security groups (without side-effects)
* Use additional parameters instead a hardcoded values
